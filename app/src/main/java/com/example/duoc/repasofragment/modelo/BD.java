package com.example.duoc.repasofragment.modelo;

import com.example.duoc.repasofragment.entidades.Producto;

import java.util.ArrayList;

/**
 * Created by Duoc on 21-10-2016.
 */
public class BD {
    private static ArrayList<Producto> productos = new ArrayList<>();

    public static boolean agregarProducto(Producto nuevoProducto){

        if(!isExisteProducto(nuevoProducto.getId())){
            productos.add(nuevoProducto);
            return true;
        }else {
            return false;
        }
    }

    public static boolean isExisteProducto(int codigo){

        for(Producto aux : productos){
            if(codigo == aux.getId()){
                return true;
            }
        }
        return false;
    }

    public static ArrayList<Producto> listarProductos(){
        return productos;
    }

    public static boolean eliminarProducto(int codigo){

        for(int x = 0 ; x < productos.size(); x++){
            if(productos.get(x).getId() == codigo) {
                productos.remove(x);
                return true;
            }
        }
        return false;
    }

   /* public static boolean eliminarProducto(int codigo){
        boolean retorno = false;

        ArrayList<Producto> arregloAux = new ArrayList<>();

        for(Producto aux : productos){
            if(aux.getId() < 5){
                arregloAux.add(aux);
            }
        }

        for(Producto aux : arregloAux){
           productos.remove(aux);
        }
        return retorno;
    }*/
}
