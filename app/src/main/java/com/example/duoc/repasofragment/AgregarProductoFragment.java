package com.example.duoc.repasofragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.repasofragment.entidades.Producto;
import com.example.duoc.repasofragment.modelo.BD;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AgregarProductoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class AgregarProductoFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Button btnAgregarProducto;
    private EditText txtCodigo, txtNombre, txtDescripcion;

    public AgregarProductoFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_agregar_producto, container, false);
        btnAgregarProducto = (Button) v.findViewById(R.id.btnAgregarProducto);
        btnAgregarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarProducto();
            }
        });
        txtCodigo = (EditText) v.findViewById(R.id.txtCodigo);
        txtNombre = (EditText) v.findViewById(R.id.txtNombre);
        txtDescripcion = (EditText) v.findViewById(R.id.txtDescripcion);
        return v;
    }

    private void agregarProducto() {
        Producto p = new Producto(Integer.parseInt(txtCodigo.getText().toString()),
                txtNombre.getText().toString(),
                txtDescripcion.getText().toString());
        if(BD.agregarProducto(p)){
            Toast.makeText(getActivity(), "Producto Agregado :)", Toast.LENGTH_LONG).show();
            txtCodigo.setText("");
            txtNombre.setText("");
            txtDescripcion.setText("");
        }else{
            Toast.makeText(getActivity(), "Código ya existe :(", Toast.LENGTH_LONG).show();
            txtCodigo.requestFocus();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
