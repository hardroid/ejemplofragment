package com.example.duoc.repasofragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends BaseFragment implements View.OnClickListener {

    private Button btnAgregar, btnModificar, btnListar, btnSalir;


    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, container, false);

        btnAgregar = (Button)v.findViewById(R.id.btnAgregar);
        btnModificar = (Button)v.findViewById(R.id.btnModificar);
        btnListar = (Button)v.findViewById(R.id.btnListar);
        btnSalir = (Button)v.findViewById(R.id.btnSalir);

        //Implementar interfaz OnClickListener de forma anonima
       /* btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        btnAgregar.setOnClickListener(this);
        btnModificar.setOnClickListener(this);
        btnListar.setOnClickListener(this);
        btnSalir.setOnClickListener(this);






        return v;
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnAgregar:
                cambiarFragment(new AgregarProductoFragment(), "agregar");
                break;
            case R.id.btnModificar:
                cambiarFragment(new ModificarProductoFragment(), "modificar");
                break;
            case R.id.btnListar:
                cambiarFragment(new ListarProductoFragment(), "listar");
                break;
            case R.id.btnSalir:
                getActivity().finish();
                break;
        }

        /*if(v.getId() == R.id.btnAgregar){
            Toast.makeText(getActivity(), "Este es un click de agregar", Toast.LENGTH_SHORT).show();
        }else if(v.getId() == R.id.btnModificar){
            Toast.makeText(getActivity(), "Este es un click de modificar", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getActivity(), "Este es un click", Toast.LENGTH_SHORT).show();
        }*/
    }
}
