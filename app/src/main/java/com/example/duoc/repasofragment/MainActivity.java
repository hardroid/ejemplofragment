package com.example.duoc.repasofragment;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.duoc.repasofragment.entidades.Producto;
import com.example.duoc.repasofragment.modelo.BD;

public class MainActivity extends AppCompatActivity implements AgregarProductoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Producto p = new Producto(1,"fvsd","fsdfs");
        Producto p2 = new Producto(2,"fvsd","fsdfs");
        Producto p3 = new Producto(3,"fvsd","fsdfs");
        Producto p4 = new Producto(4,"fvsd","fsdfs");
        Producto p10 = new Producto(10,"fvsd","fsdfs");
        BD.agregarProducto(p);
        BD.agregarProducto(p2);
        BD.agregarProducto(p3);
        BD.agregarProducto(p4);
        BD.agregarProducto(p10);
        BD.eliminarProducto(2);
        BD.listarProductos();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(this, "Este es un click de mi fragmento", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
