package com.example.duoc.repasofragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by Duoc on 21-10-2016.
 */
public class BaseFragment extends Fragment {

    public void cambiarFragment(Fragment fragment, String tag){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace( R.id.contentPanel,fragment , tag);
        ft.addToBackStack(null);
        ft.commit();
    }
}
